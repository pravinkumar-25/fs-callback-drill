const fs = require("fs");

function operationsOnFile(filePath, callback) {

    if (!callback || typeof callback !== 'function') {
        console.log("Callback is not a function");
    } else if (!filePath || typeof filePath !== 'string') {
        callback("Filepath is not defined properly");
    } else {

        fs.readFile(filePath, 'utf8', (err, content) => {
            if (err) {
                callback(err);
            } else {
                try {

                    setTimeout(() => {
                        let upperCaseContent = content.toUpperCase();
                        let upperCaseFile = "upperCaseContent.txt";
                        fs.writeFile("output/" + upperCaseFile, upperCaseContent, 'utf8', (err) => {
                            if (err) {
                                callback(err);
                            } else {
                                fs.writeFile("output/filenames.txt", upperCaseFile, 'utf8', (err) => {
                                    if (err) {
                                        callback(err);
                                    } else {
                                        fs.readFile("output/" + upperCaseFile, 'utf8', (err, content) => {
                                            if (err) {
                                                callback(err);
                                            } else {
                                                let lowerCaseContent = content.toLowerCase();
                                                let lowerCaseSplitFile = "lowerCaseContent.txt";
                                                let lowerCaseSentence = lowerCaseContent.replace("\n\n", ' ').replace("\n\n", " ").split(". ");
                                                fs.writeFile("output/" + lowerCaseSplitFile, JSON.stringify(lowerCaseSentence), 'utf8', (err) => {
                                                    if (err) {
                                                        callback(err);
                                                    } else {
                                                        fs.appendFile("output/filenames.txt", "," + lowerCaseSplitFile, 'utf8', (err) => {
                                                            if (err) {
                                                                callback(err);
                                                            } else {
                                                                fs.readFile("output/" + lowerCaseSplitFile, 'utf8', (err, data) => {
                                                                    if (err) {
                                                                        callback(err);
                                                                    } else {
                                                                        let content = JSON.parse(data);
                                                                        let sortedContent = content.sort();
                                                                        let sortedFile = "sortedContent.txt";
                                                                        fs.writeFile("output/" + sortedFile, JSON.stringify(sortedContent), 'utf8', (err) => {
                                                                            if (err) {
                                                                                callback(err);
                                                                            } else {
                                                                                fs.appendFile("output/filenames.txt", "," + sortedFile, 'utf8', (err) => {
                                                                                    if (err) {
                                                                                        callback(err);
                                                                                    } else {
                                                                                        fs.readFile("output/filenames.txt", 'utf8', (err, files) => {
                                                                                            if (err) {
                                                                                                callback(err);
                                                                                            } else {
                                                                                                setTimeout(() => {
                                                                                                    let fileNames = files.split(",");
                                                                                                    let executedTimes = 0;
                                                                                                    let errors = [];
                                                                                                    let deletedFiles = [];
                                                                                                    let numberOfFiles = fileNames.length;
                                                                                                    fileNames.forEach(file => {
                                                                                                        fs.unlink("output/" + file, (err) => {
                                                                                                            executedTimes += 1;
                                                                                                            if (err) {
                                                                                                                errors.push(err);
                                                                                                            } else {
                                                                                                                deletedFiles.push(file);
                                                                                                            }
                                                                                                            if (executedTimes === numberOfFiles) {
                                                                                                                if (errors.length > 0) {
                                                                                                                    callback(errors);
                                                                                                                } else {
                                                                                                                    callback(null, "Created and deleted files");
                                                                                                                }
                                                                                                            }
                                                                                                        })
                                                                                                    });
                                                                                                }, 2 * 1000);

                                                                                            }
                                                                                        })
                                                                                    }
                                                                                })
                                                                            }
                                                                        })
                                                                    }
                                                                })
                                                            }
                                                        })
                                                    }
                                                })
                                            }
                                        })
                                    }
                                })
                            }
                        })
                    }, 2 * 1000);

                } catch (error) {
                    callback(error);
                }
            }
        })
    }
}

module.exports = operationsOnFile;