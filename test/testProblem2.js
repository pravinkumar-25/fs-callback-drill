const operationsOnFile = require("../problem2");

//veryfing that the code works
operationsOnFile("./lipsum.txt", (err, success) => {
    if (err) {
        console.error("An error occurred");
        console.log(err);
    } else {
        console.log("Operation completed");
        console.log(success);
    }
})


//verifying that the code doesn't work, when wrong file path is passed
operationsOnFile("./lipsumss.txt", (err, success) => {
    if (err) {
        console.error("An error occurred");
        console.log(err);
    } else {
        console.log("Operation completed");
        console.log(success);
    }
})

//verifying that the code doesn't work, when callback function is undefined
operationsOnFile("./lipsum.txt")
