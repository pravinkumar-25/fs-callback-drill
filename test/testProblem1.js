const generateAndDeleteJsonFiles = require("../problem1");

//verifying that the code works perfectly
generateAndDeleteJsonFiles("./Random",(err,message)=>{
    if(err){
        console.log("Failure");
        console.error(err);
    }else{
        console.log(message);
    }
})

//verifying that the code doesn't work, when directory path is undeifined
generateAndDeleteJsonFiles(null,(err,message)=>{
    if(err){
        console.log("Failure");
        console.error(err);
    }else{
        console.log(message);
    }
})

//verifying that the code doesn't work, when callback function is not deffined properly
generateAndDeleteJsonFiles("./Random")

//verifying that the code doesn't work when string is passed instead of callback function
generateAndDeleteJsonFiles("./Random","Hello");