const fs = require("fs");

function generateAndDeleteJsonFiles(directoryPath, callback) {
    if (!callback || typeof callback !== 'function') {
        console.error("Callback function is not defined properly");
    }
    else if (!directoryPath || typeof directoryPath !== "string") {
        callback("Directory is not defined properly");
    } else {
        setTimeout(() => {
            fs.mkdir(directoryPath, { recursive: true }, (err) => {
                if (err) {
                    callback(err);
                } else {
                    try {

                        let numberOfFiles = 10;
                        let jsonData = {
                            name: "Pravin",
                            location: "Theni"
                        };
                        let success = [];
                        let errros = [];
                        let executedTimes = 0;
                        
                        for (let files = 0; files < numberOfFiles; files++) {
                            let filePath = directoryPath + "/random" + (files + 1) + ".json";
                            fs.writeFile(filePath, JSON.stringify(jsonData), 'utf8', (err) => {
                                executedTimes += 1;
                                if (err) {
                                    errros.push(err);
                                }else{
                                    success.push(filePath);
                                }
                                if(executedTimes === numberOfFiles){
                                    if(errros.length > 0){
                                        callback(errros);
                                    }else{
                                        fs.readdir(directoryPath, (err, filesArray) => {
                                            if (err) {
                                                callback(err);
                                            } else {
                                                setTimeout(() => {
                                                    let removeErrors = [];
                                                    let deletedTimes = 0;
                                                    let execute = 0;
                                                    filesArray.forEach(file => {
                                                        let removeFile = directoryPath + "/" + file;
                                                        fs.unlink(removeFile, (err) => {
                                                            execute += 1;
                                                            if (err) {
                                                                removeErrors.push(err);
                                                            }else{
                                                                deletedTimes += 1;
                                                            }
                                                            if(execute === numberOfFiles){
                                                                if(removeErrors.length > 0){
                                                                    callback(removeErrors);
                                                                }else{
                                                                    callback(null,"Created and deleted files successfully");
                                                                }
                                                            }
                                                        })
                                                    });
                                                    
                                                }, 2 * 1000)
                                            }
                                        })
                                    }
                                }
                            })
                        }

                    } catch (error) {
                        callback(error);
                    }
                }
            })
        }, 2 * 1000)

    }
}

module.exports = generateAndDeleteJsonFiles;